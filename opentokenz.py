import os
import json
import requests
import nltk
from nltk.tokenize import word_tokenize
nltk.download('punkt')

class ChatGPTFormatter():
    def __init__(self, system_message, history_file='conversation_history.json', token_limit=2048):
        self.system_message = system_message
        self.history_file = history_file
        self.token_limit = token_limit
        self.conversation_history = self.load_history()

    def load_history(self):
        if os.path.exists(self.history_file):
            with open(self.history_file, 'r') as f:
                return json.load(f)
        else:
            return []

    def save_history(self):
        with open(self.history_file, 'w') as f:
            json.dump(self.conversation_history, f, indent=2)

    def add_message(self, user, message):
        self.conversation_history.append({"user": user, "message": message})
        self.save_history()

    def limit_history_by_tokens(self, history):
        tokens = 0
        for i, entry in enumerate(reversed(history)):
            user, message = entry['user'], entry['message']
            #tokens += len(list(self.tokenizer.tokenize(f"{user}: {message}\n")))
            tokens += len(list(word_tokenize(f"{user}: {message}\n")))
            if tokens > self.token_limit:
                return history[-(i):]
        return history

    def format_input_for_chatgpt(self):
        conversation = f"{self.system_message}\n"
        for entry in self.conversation_history:
            user, message = entry['user'], entry['message']
            conversation += f"{user}: {message}\n"
        
        return conversation
    
    def gpt_prep(self):
        hist = self.limit_history_by_tokens(self.conversation_history)
        full_data = [
            {
            "role":"system","content": self.system_message
            }
        ]
        for hist_line in hist:
            if(hist_line['message']==''):
                pass
            elif(hist_line['user']=='assistant'):
                new_line = {"role":"assistant","content": hist_line['message']}
            else:
                new_line = {"role":"user","content": hist_line['message']}
            if(hist_line):
                full_data.append(new_line)
        chatgptm = {'prompt':json.dumps(full_data)}
        return chatgptm

    def gpt_send(self,chatgptm):
        chatres = requests.post('http://localhost:5100',data=chatgptm)
        chatdec = json.loads(chatres.text)
        chatmsg = chatdec['choices'][0]['message']['content']
        return chatmsg
