import tkinter as tk
from PIL import Image, ImageTk, ImageFile
import pygetwindow as gw
import json
import ctypes
import io
import os
from pathlib import Path
import webbrowser
import spotipy
import spotipy.util as util
from config import config,settings
import sched
import time
import urllib
from maidmenus import MenuConfig, ConfigurableMenu, DropdownMenu

ImageFile.LOAD_TRUNCATED_IMAGES = True
os.chdir(Path(__file__).parent)
theme = settings['settings']['theme']
theme_config = json.load(open('./themes/' + theme + '/theme_config.json','r'))
music_title = theme_config['music_title']

new_song = ''
song_sec = '00'
song_min = '0'
spot_img_set = True
current_track = None
s = sched.scheduler(time.time, time.sleep)
scope = 'user-read-currently-playing'

def spotifytoken():
    token = util.prompt_for_user_token(config['SPOTIFY_USERNAME'], scope,
                                   config['SPOTIFY_CLIENT_ID'],client_secret=config['SPOTIFY_CLIENT_SECRET'],
                                   redirect_uri='http://localhost:8888/callback')
    spotify = spotipy.Spotify(auth=token)
    return spotify
spotify = spotifytoken()

# Constants
TRANSPARENT_COLOR = theme_config['transparent_color']
song_URL = 'https://spotify.com'

# Global variables
start_x = 0
start_y = 0
result = 0


# Tools

def convert_from_ms( milliseconds ): 
    seconds, milliseconds = divmod(milliseconds,1000) 
    minutes, seconds = divmod(seconds, 60) 
    return str(minutes),str(seconds).zfill(2)


# Functions



def start_drag(event):
    global start_x, start_y
    start_x = event.x
    start_y = event.y

def drag(event):
    global start_x, start_y
    x = win.winfo_x() + (event.x_root - start_x)
    y = win.winfo_y() + (event.y_root - start_y)
    start_x, start_y = event.x_root, event.y_root
    win.geometry(f'+{x}+{y}')


def on_click(event):
    global start_x, start_y
    start_x, start_y = win.winfo_pointerx(), win.winfo_pointery()
    x, y = event.x, event.y
    if pil_image.mode == 'RGBA':
        r, g, b, a = pil_image.getpixel((x, y))
        if a > 0:
            win.bind('<B1-Motion>', drag)
        else:
            win.unbind('<B1-Motion>')

def on_release(event):
    win.unbind('<B1-Motion>')

def open_url(event,URLx):
    webbrowser.open(URLx)

def close_window(event):
    quit()

def change_text(event,tkinput,newtext):
    tkinput.config(text = newtext)

    


# Create window
win = tk.Tk()
win.overrideredirect(1)
win.wm_attributes('-transparentcolor', TRANSPARENT_COLOR)
win.attributes('-topmost', 1)

#Ticke handler

def image_widget(filename : str,ix,iy,size_x,size_y):
    pil_image = Image.open(filename)
    if not size_x == 0 or not size_y == 0:
        pil_image = pil_image.resize((size_x,size_y))
    tk_image = ImageTk.PhotoImage(pil_image)
    label = tk.Label(win, image=tk_image, bg=TRANSPARENT_COLOR,borderwidth=0)
    if ix == 0 and iy == 0:
        label.pack()
    else:
        label.place(x=ix,y=iy)
    return pil_image,tk_image,label

def image_from_url(iurl,label):
    raw_data = urllib.request.urlopen(iurl).read()
    im = Image.open(io.BytesIO(raw_data))
    im = im.resize((theme_config['spotify']['size_x'],theme_config['spotify']['size_y']))
    image = ImageTk.PhotoImage(im)
    label.config(image=image)
    label.image = image
 
def image_from_base(fileloc,label):
    im = Image.open(fileloc)
    im = im.resize((theme_config['spotify']['size_x'],theme_config['spotify']['size_y']))
    image = ImageTk.PhotoImage(im)
    label.config(image=image)
    label.image = image

def text_widget(tstring : str, tx, ty,ix,iy):
    tk_text = tk.Text(win, height = tx, width = ty)
    tx_label = tk.Label(win, text = tstring, bg="black", fg="#666666")
    tx_label.config(font =(":" + music_title['font'],music_title['font_size']))
    tx_label.place(x=ix,y=iy)
    tk_text.insert(tk.END, 'some other string')
    return tk_text,tx_label
    
def tock():
    global song_sec
    song_sec = str(1 + int(song_sec)).zfill(2)
    return song_sec
def tick(text_label,label_song):
    global current_track
    global spotify
    global new_song
    global song_sec
    global song_min
    global spot_img_set
    song_sec = tock()
    if song_sec[-1] == '0' or song_sec[-1] == '5' or current_track == None:
        try:
            current_track = spotify.current_user_playing_track()
            if(current_track is not None):
                song_min, song_sec =convert_from_ms(current_track['progress_ms'])
        except spotipy.SpotifyOauthError as e:
            spotify = spotifytoken() #Generate a new token if it expires
    
    if(current_track is not None):
        track_string = current_track['item']['artists'][0]['name'] + ' - ' + current_track['item']['name']
    else:
        track_string = ''
    if(track_string != new_song and current_track is not None):
        new_song = track_string
        image_from_url(current_track['item']['album']['images'][1]['url'],label_song)
        song_URL = current_track['item']['external_urls']['spotify']
        label_song.bind('<Button-1>', lambda event: open_url(event,song_URL))
        spot_img_set = True
    elif current_track is None:
        if(spot_img_set == True):
            image_from_base('./themes/' + theme + '/images/' + theme_config['spotify']['image'],label_song)
            spot_img_set = False
    if current_track is not None:
        track_string_timed = track_string + ' ' + song_min + ':' + song_sec
    else:
        track_string_timed = ''
    text_label.config(text = str(track_string_timed))
    win.after(1000, lambda: tick(text_label,label_song)) # after 1,000 milliseconds, call tick() again


# Load images



labels = {}
pils = {}
tkimgs = {}
label_index = str(0)
for widget in theme_config['widgets']:
    if widget['type'] ==  'main':
        pil_image,tk_image,main_label = image_widget('./themes/' + theme + '/images/' + widget['image'],widget['x'],widget['y'],widget['size_x'],widget['size_y'])
    elif widget['type'] == 'url_button':
        pils[label_index],tkimgs[label_index],labels[label_index] = image_widget('./themes/' + theme + '/images/' + widget['image'],widget['x'],widget['y'],widget['size_x'],widget['size_y'])
        labels[label_index].bind('<Button-1>', lambda event,label_index=label_index: open_url(event,theme_config['widgets'][int(label_index)]['url']))
    elif widget['type'] == 'spotify_album':
        song_pil,song_tk,label_song = image_widget('./themes/' + theme + '/images/' + widget['image'],widget['x'],widget['y'],widget['size_x'],widget['size_y'])
    elif widget['type'] == 'close_button':
        close_pil,song_tk,label_close = image_widget('./themes/' + theme + '/images/' + widget['image'],widget['x'],widget['y'],widget['size_x'],widget['size_y'])
    label_index = str(1 + int(label_index))





base_text, base_label = text_widget('Music Bar',music_title['char_x'],music_title['char_y'],music_title['x'],music_title['y'])

# Bind events
main_label.bind('<Button-1>', start_drag)
main_label.bind('<ButtonRelease-1>', on_release)
main_label.bind('<Double-Button-1>', lambda e: win.destroy())
main_label.bind('<Button-1>', on_click)

label_close.bind('<Button-1>', close_window)

base_label.bind('<Button-1>', lambda event: change_text(event,base_label,"Hope you're doing okay."))



menu_x = 400
menu_y = 400
# Load menu configuration
menu_config = MenuConfig('menu_config.json')

# Create the dropdown menu
dropdown_menu = DropdownMenu(win, menu_config,menu_x,menu_y)

# Create the button
button = tk.Button(win, text="Menu", fg="grey", bg='#111111', highlightthickness=0, borderwidth=0, activebackground="#111111", activeforeground="white", bd=0)

button.place(x=menu_x, y=menu_y)

# Bind the button to show the dropdown menu
button.bind('<Button-1>', lambda event: dropdown_menu.post(event.x_root, event.y_root))



# Get window handle
win.update_idletasks()
hwnd = gw.getWindowsWithTitle(win.title())[0]._hWnd

# Apply WS_EX_LAYERED to the window
GWL_EXSTYLE = -20
WS_EX_LAYERED = 0x00080000

styles = ctypes.windll.user32.GetWindowLongW(hwnd, GWL_EXSTYLE)
styles |= WS_EX_LAYERED
ctypes.windll.user32.SetWindowLongW(hwnd, GWL_EXSTYLE, styles)

# Start the main loop
tick(base_label,label_song)
win.mainloop()
