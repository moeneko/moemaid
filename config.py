import os
import types
import json
from dotenv import dotenv_values

config = dotenv_values(".env")
settings = json.load(open('./config.json','r'))