from opentokenz import ChatGPTFormatter

system_message = 'You are a desktop virtual assistant who appears as and has the demeanor of a maid'

gpt = ChatGPTFormatter(system_message)

while True:
    in_message = input(":")
    gpt.add_message('user',in_message)
    out_message = gpt.gpt_send(gpt.gpt_prep())
    gpt.add_message('assistant',out_message)
    print(out_message)