import tkinter as tk
import json
import os
import sys
import webbrowser
import subprocess

class MenuConfig:
    def __init__(self, config_file):
        with open(config_file, "r") as f:
            self.config = json.load(f)

    def get_items(self):
        return self.config["items"]

class ConfigurableMenu:
    def __init__(self, config):
        self.config = config

class Popup(ConfigurableMenu):
    pass

class DropdownMenu(tk.Menu, ConfigurableMenu):
    def __init__(self, master, config, x, y):

        self.menu_font = ("Arial", 12)
        self.menu_bg = "#111111"
        self.menu_fg = "#888888"
        self.menu_bd = 0
        tk.Menu.__init__(self, master, tearoff=0, font=self.menu_font, bg=self.menu_bg, fg=self.menu_fg, bd=self.menu_bd, relief='flat', activeborderwidth=0)
        ConfigurableMenu.__init__(self, config)

        for item in self.config.get_items():
            menu_item = self.create_item(item)
            self.add_cascade(label=item["label"], menu=menu_item, font=self.menu_font)

        self.post(x, y)

    def create_item(self, item):
        item_label = item["label"]
        sub_items = item['subitems']
        menu_item = tk.Menu(self, tearoff=0, font=self.menu_font, bg=self.menu_bg, fg=self.menu_fg, bd=self.menu_bd, relief='flat')
        for subi in sub_items:
           menu_item.add_command(label=subi['name'], command=lambda subi=subi: self.on_item_click(subi))
        return menu_item

    def on_item_click(self, item):
        action = item["action"]
        if action == "open_application":
            exe = item["exe"]
            subprocess.Popen([exe])
        elif action == "open_website":
            url = item["url"]
            webbrowser.open(url)
        elif action == "os_function":
            acommand = item["command"]
            if acommand == 'reload':
                os.execv(sys.executable, [sys.executable] + sys.argv)
            elif acommand == 'quit':
                sys.exit()
        elif action == "print":
            print(f'{item["name"]} clicked')
